import { enableProdMode, NgModuleRef, ApplicationRef, NgZone } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { AppComponent } from './app/app.component';

if (environment.production) {
  enableProdMode();
}

// overwrite bootstring angular
platformBrowserDynamic().bootstrapModule(AppModule)
.then((moduleRef: NgModuleRef<AppModule>) => {
  // get reference to root view
  // https://angular.io/api/core/ApplicationRef
  const app: ApplicationRef = moduleRef.injector.get(ApplicationRef);

  // executes angular bootraping within the Angular zone to avoid conflicts with the DOM
  // https://angular.io/api/core/NgZone
  const ngZone: NgZone = moduleRef.injector.get(NgZone);
  ngZone.run(() => {

    // render component in a container div
    app.bootstrap(AppComponent, '#app-widget');
  });
})
.then(ref => {
  // Ensure Angular destroys itself on hot reloads.
  if (window['ngRef']) {
    window['ngRef'].destroy();
  }
  window['ngRef'] = ref;

  // Otherwise, log the boot error
}).catch(err => console.error(err));