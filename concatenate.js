const fs = require('fs-extra');
const concat = require('concat');
(async function build() {
const files = [
'./dist/widget-poc/runtime.js',
'./dist/widget-poc/polyfills.js',
'./dist/widget-poc/main.js',
]
await fs.ensureDir('website')
await concat(files, 'website/bundle.js');
})()